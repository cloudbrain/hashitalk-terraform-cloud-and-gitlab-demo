## Setting up GitLab Runners on local machine
Reference: [Install GitLab Runner for your system](https://docs.gitlab.com/runner/install/)
From GitLab CI / CD projects page, click "Set up a specific Runner manually":
  - Install GitLab runner locally on your OS
  - Use `gitlab-runner register` command to register the runner:
    - `gitlab-ci coordinator URL` should be `https://gitlab.com/` or your GitLab enterprise server
    - `gitlab-ci token` will be provided in GitLab UI (Settings > CI/CD > "Set up a specific Runner")
    - `gitlab-ci tags` should say `curl` as this is specified in `.gitlab-ci.yml` file.
    - `executor`: docker
    - `default Docker image`: Specify the Docker image you built, or use `kawsark/gitlab-terraform:0.0.3` which contains `curl`, `jq`, `vault 1.6.1` and `terraform 0.12.29` binaries.
  - Run `gitlab-runner verify` to ensure the runner is registered properly
  - Start the Runner using `gitlab-runner run`

## Cleaning up old Local runners
If you deleted Runners from the GitLab UI, run the following command to remove them from your system (see [reference](https://gitlab.com/gitlab-org/gitlab-runner/issues/2619#note_211629769))
```
gitlab-runner verify --delete
```
