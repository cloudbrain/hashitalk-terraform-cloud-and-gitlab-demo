#!/bin/bash

# **Important** VAULT_ADDR, VAULT_TOKEN, GOOGLE_PROJECT, and GOOGLE_CREDENTIALS_PATH environment variables should be set prior to running this script
# This script configures the GCP secrets Engine in Vault
# We assume the workspace name is gitlab-ci-gcp, please adjust this if needed

print_help() {
  echo "Please ensure the following Environment variables are set" 
  echo -e "\t* VAULT_ADDR and VAULT_TOKEN"
  echo -e "\t* TFC_TOKEN to store in Key/Value Secrets Engine or the TFC Secrets Engine" 
  echo -e "\t* GOOGLE_PROJECT and GOOGLE_CREDENTIALS_PATH environment variables are set."
  echo "Optionally please set these variables:"
  echo -e "\t* GITLAB_PROJECT_ID - Setup the JWT Auth method. GitLab Project ID will be configured as an assertion"
  echo -e "\t* TFC_SE - set this to True to use the TFC Secrets Engine instead of Key/Value Secrets Engine"
  echo -e "\t* TFC_SE_team_id - set this to the team ID to generate an API token for"
  echo -e "\t* TFC_SE_team_name - set this to the team name. This can be anything, default is dev-team"
}

if [[ $1 == "-help" ]] || [[ $1 == "-h" ]]; then
  print_help
  exit
fi

./check_vault_vars.sh
if [ ! -f "vars_are_valid" ]; then
    echo "ERR: Failed to verify required Variables."
    print_help
    exit 1
fi

echo "INFO: Checking connectivity to vault server: ${VAULT_ADDR}, and vault token"
curl -kv "${VAULT_ADDR}/v1/sys/health"
vault status
vault token lookup

echo "INFO: Creating the AppRole Auth Method"
vault auth enable approle
vault write auth/approle/role/gitlab \
    secret_id_ttl=24h \
    token_num_uses=50 \
    token_ttl=20m \
    token_max_ttl=30m \
    secret_id_num_uses=50 \
    policies="gitlab-runner"

vault read auth/approle/role/gitlab

# Use .data.role_id in role.json file as the ROLE_ID for Gitlab setup
echo "INFO: Role ID written to role.json file"
vault read -format=json auth/approle/role/gitlab/role-id > role.json
export ROLE_ID="$(cat role.json | jq -r .data.role_id )" && echo $ROLE_ID > roleid

# Use .data.secret_id in secretid.json file as the SECRET_ID for Gitlab credential
echo "INFO: Secret ID written to secret.json file"
vault write -format=json -f auth/approle/role/gitlab/secret-id > secretid.json
export SECRET_ID="$(cat secretid.json | jq -r .data.secret_id )" && echo $SECRET_ID > secretid

if [ ! -z "$GITLAB_PROJECT_ID" ]; then
  echo "INFO: Creating the JWT auth method"
  # Create the key/value secrets engine to store Terraform Cloud credentials
  vault auth enable jwt

  vault write auth/jwt/config \
    jwks_url="https://gitlab.com/-/jwks" \
    bound_issuer="gitlab.com"

  echo "INFO: Creating a Role for GitLab under the jwt auth method"
  cat <<EOF > gitlab-runner-role.json
  {
    "role_type": "jwt",
    "policies": ["gitlab-runner"],
    "token_explicit_max_ttl": 300,
    "user_claim": "user_email",
    "bound_claims": {
    "project_id": "$GITLAB_PROJECT_ID",
    "ref": "master",
    "ref_type": "branch"
    }
  }
EOF

vault write auth/jwt/role/gitlab @gitlab-runner-role.json

echo "NOTE: bound_claims is set to Gitlab project ID: $GITLAB_PROJECT_ID."

echo "INFO: Displaying the jwt auth method and role"
vault read auth/jwt/config
vault read auth/jwt/role/gitlab
  
else
  echo "INFO: Skipping the JWT auth method, please set a VAULT_TOKEN environment variable instead."
fi

# Setup TFC_Token as a secret
[[ -z $TFC_SE_team_name ]] && TFC_SE_team_name="dev-team"
tfc_se=False

if [[ $TFC_SE == "True" ]] && [[ ! -z $TFC_SE_team_id ]]; then
  echo "INFO: Create the TFC secrets engine"
  vault secrets enable terraform
  vault write terraform/config token=$TFC_TOKEN
  vault write terraform/role/${TFC_SE_team_name} team_id=$TFC_SE_team_id
  tfc_se="True"
else
  echo "INFO: Create the kv2 secrets engine"
  # Create the key/value secrets engine to store Terraform Cloud credentials
  vault secrets enable -path=kv2 -version=2 kv
  vault kv put kv2/tfc_token token=$TFC_TOKEN
fi

echo "INFO: Creating secrets engine"
# Create the gcp secrets engine to issue credentials that are valid for five minutes  
vault secrets enable gcp
vault write gcp/config credentials=@${GOOGLE_CREDENTIALS_PATH}
vault secrets tune -default-lease-ttl=600s -max-lease-ttl=900s gcp

echo "INFO: Creating roleset binding"
# Create role set bindings for the dynamically generated credentials
vault write gcp/roleset/gitlab \
    project="${GOOGLE_PROJECT}" \
    secret_type="service_account_key"  \
    bindings=-<<EOF
resource "//cloudresourcemanager.googleapis.com/projects/${GOOGLE_PROJECT}" {
  roles = ["roles/editor"]
}
EOF

echo "INFO: Creating policy for GitLab Runner"
# Create a policy for GitLab runners
vault policy write gitlab-runner -<<EOH
path "gcp/key/gitlab" {
  capabilities = ["read"]
}
path "kv2/data/tfc_token" {
  capabilities = ["read"]
}

# Read from terraform secrets engine
path "terraform/creds/${TFC_SE_team_name}" {
  capabilities = ["read"]
}

# Rotate terraform credentials
path "terraform/rotate-role/${TFC_SE_team_name}" {
  capabilities = ["update", "create"]
}
EOH

echo "INFO: Creating token for GitLab Runner"
# Create a token for the Runners with a TTL of 24 hours
vault token create -policy=gitlab-runner -ttl=24h

token=$(vault token create -format=json -policy=gitlab-runner -ttl=24h | jq -r .auth.client_token)
echo "INFO: Running test with token: $token"
VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab
if [[ $tfc_se == "True" ]]; then
  VAULT_TOKEN=$token vault read terraform/creds/${TFC_SE_team_name}
else
  VAULT_TOKEN=$token vault read kv2/data/tfc_token
fi

echo "INFO: Testing AppRole Auth for Gitlab Runner"
token=$(vault write -format=json auth/approle/login role_id=$ROLE_ID secret_id=$SECRET_ID | jq -r .auth.client_token)
echo "INFO: Running test with token: $token"
VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab
if [[ $tfc_se == "True" ]]; then
  VAULT_TOKEN=$token vault read terraform/creds/${TFC_SE_team_name}
else
  VAULT_TOKEN=$token vault read kv2/data/tfc_token
fi

echo "INFO: script completed. Please configure a VAULT_TOKEN variable in GitLab CI/CD Variables with value: $token"
if [[ $tfc_se == "True" ]]; then
  echo "To test: VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab && vault read terraform/creds/${TFC_SE_team_name}"
  echo "To rotate the terraform team credential: VAULT_TOKEN=$token vault write -f terraform/rotate-role/${TFC_SE_team_name}"
else
  echo "To test: VAULT_TOKEN=$token vault read -format=json gcp/key/gitlab && vault read kv2/data/tfc_token"
fi
