## Setting up GitLab Runners on Kubernetes
Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

### Install helm charts
Helm V3 is preferred as Tiller is not required.
[Install Helm for your system](https://helm.sh/docs/using_helm/#install-helm)


### Installing the GitLab Runner helm chart - quick version
If you have the `make` tool installed, along with helm V3, please use the steps below. Otherwise please see the longer version below.
```
cd helm
export RUNNER_TOKEN=<runner-registration-token from GitLab CI>
make install_runner
```

### Check pods deployed
```
kubectl get pods -n gitlab
kubectl logs -f <podname> -n gitlab
```

### Installing the GitLab Runner helm chart - longer version
- For Helm V3 please continue to next step. If you are using Helm V2, please [setup Tiller](helm/tiller.md) first. 

- Please adjust [my-values.yaml](helm/my-values.yaml) file in this repo as needed.
   - Adjust `runnerRegistrationToken` from GitLab CI / CD
   - Adjust the `image` key if you built and publised your own Docker container. The default image is `kawsark/gitlab-terraform:0.0.2`
   - Ensure the `tags` key has `curl` and any other `tags` in `.gitlab-ci.yml`.
```
cd helm/
kubectl apply -f ns.yaml 
#git clone git@gitlab.com:charts/gitlab-runner.git
helm repo add gitlab https://charts.gitlab.io
helm repo update

# Substitute <your-registration-token> below:
sed -e s/"my-runner-token"/<your-registration-token>/ < my-values-example.yaml > my-values.yaml
# Note: if the above command did not work for you, please copy the my-vaules-example.yaml file and edit it to substitue my-runner-token

# Adjust my-values.yaml file further if needed, then run
helm install gitlab-runner --namespace gitlab -f my-values.yaml gitlab/gitlab-runner
# helm V2 command:
# helm install --namespace gitlab --name gitlab-runner -f my-values.yaml gitlab/gitlab-runner
helm ls
kubectl get pods -n gitlab
```
Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)
Note: if you are getting RBAC issues during helm deployment, please consult this excellent guide [Configure RBAC for Helm](https://docs.bitnami.com/kubernetes/how-to/configure-rbac-in-your-kubernetes-cluster/#use-case-2-enable-helm-in-your-cluster).

